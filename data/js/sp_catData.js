/*
 * JS for sp_catData class
 * Used in dashboard/admin page
 */
(function($){
    var sp_catData = {

        /**
         * Display errors
         * !To-do: turn sp_admin.js into object and inherit its methods
         */
        showError: function(errorText){
            $('#setting_errors').show().html('<h4>Error: ' + errorText + '<h4>').attr("class", "error");
        },

        /**
         * Save Data component settings
         *
         * @param int compID The Data component's ID
         */
        setDataOptions: function(compID, indicatorElem){
            var self = this;
            //Setup the Data options
            var spDataOptions = {
                url	 : SP_AJAX_URL,
                type : 'POST',
                data : {action: 'saveDataSettingsAJAX', nonce: SP_ADMIN_NONCE, compID: compID},
                dataType : 'json',
                success	: function(response, statusText, xhr, $form){
                    if(response.success){
                        var success = $('<span id="successCatUpdate" style="color: green;"> Data Options saved! </span>');
                        indicatorElem.after(success); //To-do: rename indicatorElem as it's confusing where its placed
                        success.delay(3000).fadeOut().delay(3000, function(){ $(this).remove() });
                    }
                },
                error : function(data){
                    self.showError(data.statusText);
                }
            };

            return spDataOptions;
        },

        /**
         * Initializes the Data form for jquery-form submissions
         */
        bindForm: function(){
            var thisObj = this;
            $( '.update_sp_Data' ).click(function(){
                var compID       = $(this).attr('data-compid');
                var DataOptions = thisObj.setDataOptions(compID, $(this));
                $('#componentOptions-' + compID + '-form').ajaxSubmit(DataOptions);
            });
        },

        /**
         * Init method for the Data components
         */
        init: function(){
            this.bindForm();
        }
    }

    $(document).ready(function(){
        sp_catData.init();
    });
})(jQuery);