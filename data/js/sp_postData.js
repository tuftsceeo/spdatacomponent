/*
 * JS sp_postData Component class
 * Used alongside sp_postData for AJAX calls
 * Used in front-end posts
  *
 * @version 1.0
 * @author Rafi Yagudin <rafi.yagudin@tufts.edu>
 * @project SmartPost
 */
(function($){
    smartpost.sp_postData = {

//binding buttons for dataform
        bindChk: function(component, postID){
                console.log('bindChk');
                var id = component.attr('data-compid');
         //    $("#remove_chart-"+id).bind('click', function(){removeChart(id); });
             //file submission
             //$('#submit-'+id).bind('click', function(){submitClick(id); });      
             //$("#the_file-"+id).bind( 'change', function(){uploadFile(id); });

            //chk boxes
             $('#large-'+id).bind('click', function(){
                $('#medium-'+id).attr('checked',false);
                $('#small-'+id).attr('checked',false);
             });
          
             $('#medium-'+id).bind('click', function(){
                $('#small-'+id).attr('checked',false);
                $('#large-'+id).attr('checked',false);
             });
          
             $('#small-'+id).bind('click', function(){
                $('#large-'+id).attr('checked',false);
                $('#medium-'+id).attr('checked',false);
             });

               
         },
         
        /**
         * Required for all post component JS objects.
         * Used in sp_globals.types to determine which
         * methods to call for different post component types
         */
        setTypeID: function(){
            if(sp_globals){
                var types = sp_globals.types;

                //!Important - the raw name of the type
                if(types['Data']){
                    this.typeID = types['Data'];
                    sp_globals.types[this.typeID] = this;
                }
            }else{
                return 0;
            }
        },

        /**
         * Returns true if the Data component is a gallery,
         * otherwise false.
         */
        isGallery: function(compID){
            return Boolean( $('#sp_Data-' + compID).attr('data-isgallery') );
        },
        /**
         * Returns true if Data component is empty, otherwise false
         * 
         * @param object component The component
         * @return bool True if it's empty, otherwise false
         */
        isEmpty: function(component){
            var compID = $(component).attr('data-compid');
            return $(component).find('#drop_indicator-' + compID).exists();
        },

  
        initFileDrop: function(component, postID){
            console.log('initfiledrop');
            var thisObj     = this;
            var compID      = component.attr('data-compid');
            var isGallery   = this.isGallery(compID);
            var fallback_id = 'sp_upload-' + compID;
            var queueFiles  = this.isGallery(compID) ? 1 : 0;

            if(postID == undefined){
                postID = jQuery('#postID').val();
                if(postID == undefined){
                    postID = jQuery('#sp_qpPostID').val();
                }
            }

            component.filedrop({
               
                fallback_id: fallback_id,
                url: SP_AJAX_URL,
                paramname: 'sp_Data_files',
                data: {
                    action  : 'DataUploadAJAX',
                    nonce   : SP_NONCE,
                    compID  : compID,
                    postID  : postID
                },
                error: function(err, file) {
            switch(err) {
              case 'BrowserNotSupported':
                    $('.sp_browse').show();
                  break;
              case 'TooManyFiles':
                                                smartpost.sp_postComponent.showError('Too many files! Please upload less');
                  break;
              case 'FileTooLarge':
                                                smartpost.sp_postComponent.showError(file.name + ' is too large!');
                  break;
              default:
                            smartpost.sp_postComponent.showError(err);
                  break;
            }
                        $('#loadingGIF').remove();
                },
                maxfiles: 1,
                queuefiles: queueFiles,
                maxfilesize: 32, // max file size in MBs
                uploadStarted: function(i, file, len){                   
                    thisObj.loadingGif(isGallery, compID);
                },
                uploadFinished: function(i, file, response, time) {
                    if(response){
                            thisObj.insertThumb(response, isGallery, compID, $('#loadingGIF'));
                            thisObj.bindChart(response, compID);
                            //thisObj.bindSave(response, compID);
                    }
                }
            });

     },

    
     /**
      * Insert Description (used only in single-Data mode)
      * After the thumbnail div
      */
     insertDesc: function(attachmentID, compID, thumbDiv, placeHolder){
            var thisObj = this;
            var descDiv = '';

            if(thumbDiv == undefined){
                thumbDiv = $('#Data_thumb-' + attachmentID);
            }
        //needs editing
         /*   var descExists = $('#sp_attachments-' + compID).find('.sp_Data_desc');
            descDiv += '<div id="sp_Data_desc-' + attachmentID + '" class="sp_Data_desc editable" data-compid="' + compID + '" attach-id="' + attachmentID + '">';
                descDiv += placeHolder;
            descDiv += '</div>';
            if( !$(descExists).exists() ){
                thumbDiv.after( $(descDiv) );
            }else{
                $(descExists).replaceWith( $(descDiv) );
            }

            thisObj.initDescEditor($('#sp_Data_desc-' + attachmentID));*/
     },

     /**
      * Initialize the description editor
      * @param descElem jQuery <div> object of the description
      */
     initDescEditor: function(descElem){
        var thisObj = this;
            if(smartpost.sp_postComponent){
                var elementID = $(descElem).attr('id');
                    smartpost.sp_postComponent.addNicEditor(elementID, false, thisObj.saveDescription,'Click to add a description');
        }
     },

        /**
         * Saves a Data component's description to the database.
         *
         * @param string    content   The content to be saved
         * @param string    contentID The DOMElem id of the content's container
         * @param nicEditor instance  The editor instance
         */
        saveDescription: function(content, contentID, instance){
            var compID  = $('#' + contentID).attr('data-compid');
            var attachmentID  = $('#' + contentID).attr('attach-id');
            $.ajax({
                url           : SP_AJAX_URL,
                type      : 'POST',
                data         : {action: 'saveDataDescAJAX',
                                                                    nonce: SP_NONCE,
                                                                    compID: compID,
                                                                    attachmentID: attachmentID,
                                                                    desc: content},
                dataType  : 'json',
                success  : function(response, statusText, jqXHR){
                        console.log(response);
                },
                error    : function(jqXHR, statusText, errorThrown){
                        if(smartpost.sp_postComponent)
                            smartpost.sp_postComponent.showError(errorThrown);
                }
            })
        },

     /**
      * Loads a loading GIF prior to upload
      */
     loadingGif: function(isGallery, compID){
            console.log('loadingGIF isGallery: ' + isGallery)
            console.log('loadingGIF compID: ' + compID)

            var attachments = $('#sp_attachments-' + compID);
            var thumbClass = isGallery ? 'gallery_thumb' : 'sp_Data_thumb';
            var loadingGIF = '<div id="loadingGIF" class="' + thumbClass + '"><img src="' + SP_IMAGE_PATH + '/loading.gif" /></div>';

            if(isGallery || (attachments.children().length == 0) ){
                $('#sp_attachments-' + compID).append(loadingGIF);
            }else{
                $('#sp_attachments-' + compID).find('.sp_Data_thumb').replaceWith(loadingGIF);
            }
     },
        /**
         * Inserts a thumbnail into the DOM based off of info in response
         * reponse.id -> Attachment ID of the attachment
         * response.thumbURL -> Array containting [0] URL of the thumb attachment, [1] width, and [2] height
         * response.fileURL  -> String of the direct URL to the file attachments
         * response.caption  -> String caption of the thumbnail
         *
         * @param object response Valid JSON object
         * @gallery bool whether to render in gallery mode or not
         * @compID int the component ID
         * @replaceMe $(elem) jQuery element to replace the thumb with
         */
        insertThumb: function(response, isGallery, compID, replaceMe){
                var thisObj      = this;
                var thumbDiv       = '';
                var thumbClass   = isGallery ? 'gallery_thumb' : 'sp_Data_thumb';
                var deleteButton = '<img src="' + SP_IMAGE_PATH + '/no.png" id="deleteThumb-' + response.id + '" name="deleteThumb-' + response.id + '" data-attachid="' + response.id + '" data-compid="' + compID + '" class="sp_DataDelete" title="Delete Attachment" alt="Delete Attachment" />';
                var captionEl    = '<p id="Data_caption-' + response.id +'" class="sp_DataCaption">' + response.caption + '</p>';

                thumbDiv += '<div id="Data_thumb-' + response.id + '" data-compid="704" class="' + thumbClass + '">';
                    thumbDiv += '<a id="thumb-' + response.id +'" href="' + response.fileURL + '" rel="lightbox[' + compID + ']" title="' + response.caption + '">';
                        thumbDiv += '<img width="' + response.thumbURL[1] + '" height="' + response.thumbURL[2] + '" src="' + response.thumbURL[0] + '" class="attachment-100x100">';
                    thumbDiv += '</a>';
                    thumbDiv += captionEl;
                    thumbDiv += deleteButton;
                thumbDiv += '</div>';

                replaceMe.replaceWith($(thumbDiv));
                thisObj.bindDelete($('#deleteThumb-' + response.id));
                thisObj.bindDeleteHover($('#Data_thumb-' + response.id));

                if(!isGallery)
                    thisObj.insertDesc(response.id, compID);

                return $(thumbDiv);
        },
        /**
        * bind chart buttons after
        * a file has been uploaded
        *
        */
        bindChart: function(response, compID){
                   console.log('chartbound');
            //chart creators
            $('#piechart_b-'+compID).bind('click', function(){
         
            
           //<canvas id=the_chart-+compID+" width="470px" height="470px"></canvas>style="DISPLAY:none"
            d3.select("#chart_wrapper-" + compID).html('<div class="button_s remove_post" id="remove_post-'+compID+'" >Save Chart!</div><div id="the_chart-'+compID+'"></div><canvas id="hidden_chart-'+compID+'" style="DISPLAY:none" width="470px" height="470px"></canvas>');
             $("#remove_post-"+compID).bind("click", function(){
               svgcode = $("#the_chart-"+compID).html().trim();     
               jsoncode = JSON.stringify(svgcode);
               console.log(jsoncode);
               //  // write the svg to the canvas
               //  var canvas = document.getElementById("hidden_chart-"+compID);             
               //  canvg(canvas, $svgcode);
                
               //  // create an png of the canvas
               //  var img = canvas.toDataURL("image/png");
               //  console.log($svgcode);
               // console.log(img);
                $.ajax({
                    url           : SP_AJAX_URL,
                    type      : 'POST',
                    data         : {
                    json: jsoncode,
                    action: 'saveChartAJAX',
                    nonce: SP_NONCE,
                    compID: compID, },
                    success  : function(response, statusText, jqXHR){
                            console.log(response);
                    },
                    error    : function(jqXHR, statusText, errorThrown){
                            if(smartpost.sp_postComponent)
                                smartpost.sp_postComponent.showError(errorThrown);
                    }
                })


             });

                if( $("#large-"+compID).is(":checked")){var radius = 150;}                  
                if( $("#medium-"+compID).is(":checked")){var radius = 74;}              
                if( $("#small-"+compID).is(":checked")){var radius = 50;}
                
                var padding = 10;

                var color = d3.scale.ordinal()
                    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

                var arc = d3.svg.arc()
                    .outerRadius(radius)
                    .innerRadius(radius*0.4);

                var pie = d3.layout.pie()
                    .sort(null)
                    .value(function(d) { return d.population; });

                d3.csv(response.fileURL, function(error, data) {

                    if( error != null){     
                        document.getElementById("result").innerHTML = '"ERROR: no current file"';
                    }
                    // console.log(d3.keys(data[0])[0]);
                    else{
                      color.domain(d3.keys(data[0]).filter(function(key) { return key !== d3.keys(data[0])[0]; }));
                     
                    data.forEach(function(d) {
                        
                        d.quantities = color.domain().map(function(name) {
                            // console.log({name: name, population: +d[name]});
                          return {name: name, population: +d[name]};
                        });
                        
                        d.categories = d3.values(d)[0];
                       
                    });

                      var legend = d3.select("#the_chart-"+compID).append("svg")
                          .attr("class", "legend")
                          .attr("width", radius * 2)
                          .attr("height", radius * 2)
                        .selectAll("g")
                          .data(color.domain().slice().reverse())
                        .enter().append("g")
                          .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

                      legend.append("rect")
                          .attr("width", 18)
                          .attr("height", 18)
                          .style("fill", color);

                      legend.append("text")
                          .attr("x", 24)
                          .attr("y", 9)
                          .attr("dy", ".35em")
                          .text(function(d) { return d; });

                      var svg = d3.select("#the_chart-"+compID).selectAll(".pie")
                          .data(data)
                        .enter().append("svg")
                          .attr("class", "pie")
                          .attr("width", radius * 2)
                          .attr("height", radius * 2)
                        .append("g")
                          .attr("transform", "translate(" + radius + "," + radius + ")");

                      svg.selectAll(".arc")
                          .data(function(d) { return pie(d.quantities); })
                        .enter().append("path")
                          .attr("class", "arc")
                          .attr("d", arc)
                          .style("fill", function(d) { return color(d.data.name); });

                      svg.append("text")                      
                          .attr("dy", ".35em")
                          .style("text-anchor", "middle")                 
                          .text(function(d) {
                            // console.log(d3.keys(data[0])[0]);
                            // console.log(d3.keys(d)[0]);
                            // console.log(d.categories);
                            // category = d3.keys(data[0])[0];                                  
                            return d.categories;});
                    }


                  //   // Get the d3js SVG element
                  // var tmp = document.getElementById("hidden_chart-"+compID);
                  // var svg_code = tmp.getElementsByTagName("svg")[0];
                  // // Extract the data as SVG text string
                  // var svg_xml = (new XMLSerializer).serializeToString(svg_code);
                  
                  // // Submit the <FORM> to the server.
                  // // The result will be an attachment file to download.
                  // var file = document.getElementById("sp_Chartupload-"+compID);              
                  // file.value = svg_xml;
                  // console.log(file.value);
                  // file.submit();
                   
                });
                    
          
            });



            $('#barchart_b-'+compID).bind('click', function(){

           //<canvas id=the_chart-+compID+" width="470px" height="470px"></canvas>style="DISPLAY:none"
            d3.select("#chart_wrapper-" + compID).html('<div class="button_s remove_post" id="remove_post-'+compID+'" >Save Chart!</div><div id="the_chart-'+compID+'"></div><canvas id="hidden_chart-'+compID+'" style="DISPLAY:none" width="470px" height="470px"></canvas>');
             $("#remove_post-"+compID).bind("click", function(){
                 $svgcode = $("#the_chart-"+compID).html();     
                // write the svg to the canvas
                var canvas = document.getElementById("hidden_chart-"+compID);             
                canvg(canvas, $svgcode);
                
                // create an png of the canvas
                var img = canvas.toDataURL("image/png");
                console.log($svgcode);
               console.log(img);
                // $.ajax({
                //     url           : SP_AJAX_URL,
                //     type      : 'POST',
                //     data         : {action: 'saveChartAJAX',
                //     nonce: SP_NONCE,
                //     compID: compID, },
                //     success  : function(response, statusText, jqXHR){
                //             console.log(response);
                //     },
                //     error    : function(jqXHR, statusText, errorThrown){
                //             if(smartpost.sp_postComponent)
                //                 smartpost.sp_postComponent.showError(errorThrown);
                //     }
                // })
                

             });

                if( $('#large-'+compID).is(':checked')){var w = 900, h=520;}                      
                if( $('#medium-'+compID).is(':checked')){var w = 600, h=346.666666667;}     
                if( $('#small-'+compID).is(':checked')){var w = 300, h=173.333333333;}
                        

                var margin = {top: 50, right: 80, bottom: 30, left: 80},
                margin2 = {top: 430, right: 10, bottom: 20, left: 40},
                    width = w - margin.left - margin.right,
                    height = h - margin.top - margin.bottom,
                    height2 = h - margin2.top - margin2.bottom;
                var x0 = d3.scale.ordinal()
                    .rangeRoundBands([0, width], .1);
                var x1 = d3.scale.ordinal();
                var y = d3.scale.linear()
                    .range([height, 0]);
                var color = d3.scale.ordinal()
                    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
                var xAxis = d3.svg.axis()
                    .scale(x0)
                    .orient("bottom");
                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickFormat(d3.format(".2s"));
                var svg = d3.select("#the_chart-"+compID).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                  .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                 svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                  .append("rect")
                    .attr("width", width)
                    .attr("height", height);
                d3.csv(response.fileURL, function(error, data) {
                    if( error != null){
                            document.getElementById('result').innerHTML = 'ERROR: no current file';
                    }
                    else{
                      var ageNames = d3.keys(data[0]).filter(function(key) { return key !== d3.keys(data[0])[0]; });
                      data.forEach(function(d) {
                        d.quantities = ageNames.map(function(name) { return {name: name, value: +d[name]}; });
                        d.categories = d3.values(d)[0];
                      });
                      x0.domain(data.map(function(d) { return d.categories; }));//category
                      x1.domain(ageNames).rangeRoundBands([0, x0.rangeBand()]);
                      y.domain([0, d3.max(data, function(d) { return d3.max(d.quantities, function(d) { return d.value; }); })]);
                      svg.append("g")
                          .attr("class", "x axis")
                          .attr("transform", "translate(0," + height + ")")
                          .call(xAxis);
                      svg.append("g")
                          .attr("class", "y axis")
                          .call(yAxis)
                        .append("text")
                          .attr("transform", "rotate(-90)")
                          .attr("y", 6)
                          .attr("dy", ".71em")
                          .style("text-anchor", "end");              
                          // .text(document.getElementById("y_axis").value);
                      var name = svg.selectAll(".name")
                          .data(data)
                        .enter().append("g")
                          .attr("class", "g")
                          .attr("transform", function(d) { return "translate(" + x0(d.categories) + ",-5)"; });//category
                      name.selectAll("rect")
                          .data(function(d) { return d.quantities; })
                        .enter().append("rect")
                          .attr("width", x1.rangeBand())
                          .attr("x", function(d) { return x1(d.name); })
                          .attr("y", function(d) { return y(d.value); })
                          .attr("height", function(d) { return height - y(d.value); })
                          .style("fill", function(d) { return color(d.name); });
                      var legend = svg.selectAll(".legend")
                          .data(ageNames.slice().reverse())
                        .enter().append("g")
                          .attr("class", "legend")
                          .attr("transform", function(d, i) { return "translate(75," + i * 20 + ")"; });
                      legend.append("rect")
                          .attr("x", width - 18)
                          .attr("width", 18)
                          .attr("height", 18)
                          .style("fill", color);
                      legend.append("text")
                          .attr("x", width - 24)
                          .attr("y", 9)
                          .attr("dy", ".35em")
                          .style("text-anchor", "end")
                          .text(function(d) { return d; });
                          svg.append("text")
                        .attr("x", (width / 2))             
                        .attr("y", 0 - (margin.top / 2))
                        .attr("text-anchor", "middle")
                        .style("font-family","arial black")  
                        .style("font-size", "16px")
                        .style("text-decoration", "underline")  
                        .text($('#chart_t-'+compID).val());
                    }
                });
            });

            $('#linechart_b-'+compID).bind('click', function(){
                
            
           //<canvas id=the_chart-+compID+" width="470px" height="470px"></canvas>style="DISPLAY:none"
            d3.select("#chart_wrapper-" + compID).html('<div class="button_s remove_post" id="remove_post-'+compID+'" >Save Chart!</div><div id="the_chart-'+compID+'"></div><canvas id="hidden_chart-'+compID+'" style="DISPLAY:none" width="470px" height="470px"></canvas>');
             $("#remove_post-"+compID).bind("click", function(){
                 $svgcode = $("#the_chart-"+compID).html();     
                // write the svg to the canvas
                var canvas = document.getElementById("hidden_chart-"+compID);             
                canvg(canvas, $svgcode);
                
                // create an png of the canvas
                var img = canvas.toDataURL("image/png");
                console.log($svgcode);
               console.log(img);
                // $.ajax({
                //     url           : SP_AJAX_URL,
                //     type      : 'POST',
                //     data         : {action: 'saveChartAJAX',
                //     nonce: SP_NONCE,
                //     compID: compID, },
                //     success  : function(response, statusText, jqXHR){
                //             console.log(response);
                //     },
                //     error    : function(jqXHR, statusText, errorThrown){
                //             if(smartpost.sp_postComponent)
                //                 smartpost.sp_postComponent.showError(errorThrown);
                //     }
                // })
                

             });


                if( $('#large-'+compID).is(':checked')){var w = 900, h=520;}
                if( $('#medium-'+compID).is(':checked')){var w = 600, h=346.666666667;}
                if( $('#small-'+compID).is(':checked')){var w = 300, h=173.333333333;}


                var margin = {top: 50, right: 80, bottom: 30, left: 80},
                    width = w - margin.left - margin.right,
                    height = h - margin.top - margin.bottom;

                var x = d3.scale.ordinal()
                    .rangeRoundBands([0, width], .1);

                var y = d3.scale.linear()
                    .range([height, 0]);

                var color = d3.scale.category10();

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickFormat(d3.format(".2s"));;

                var line = d3.svg.line()
                    .interpolate("basis")
                    .x(function(d) { return x(d.category); })
                    .y(function(d) { return y(d.quantity); });

                var svg = d3.select("#the_chart-"+compID).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                  .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                d3.csv(response.fileURL, function(error, data) {

                    if( error != null){
                            document.getElementById('result').innerHTML = 'ERROR: no current file';
                    }
                    else{   
                      color.domain(d3.keys(data[0]).filter(function(key) { return key !== d3.keys(data[0])[0]; }));

                    data.forEach(function(d) {
                        d.category = d3.values(d)[0];
                    });
                      var categories = color.domain().map(function(name) {
                        return {
                          name: name,
                          values: data.map(function(d) {
                            return {category: d.category, quantity: +d[name]};
                          })
                        };
                      });

                      x.domain(data.map(function(d) { return d.category; }));

                      y.domain([
                        d3.min(categories, function(c) { return d3.min(c.values, function(v) { return v.quantity; }); }),
                        d3.max(categories, function(c) { return d3.max(c.values, function(v) { return v.quantity; }); })
                      ]);

                      svg.append("g")
                          .attr("class", "x axis")
                          .attr("transform", "translate(-26," + (height +10) + ")")
                          .call(xAxis);

                      svg.append("g")
                          .attr("class", "y axis")
                          .call(yAxis)
                        .append("text")
                          .attr("transform", "rotate(-90)")
                          .attr("y", 6)
                          .attr("dy", ".71em")
                          .style("text-anchor", "end");
                          // .text($("#y_axis").val);

                      var names = svg.selectAll(".names")
                          .data(categories)
                        .enter().append("g")
                          .attr("class", "names");

                      names.append("path")
                          .attr("class", "line")
                          .attr("d", function(d) { return line(d.values); })
                          .style("stroke", function(d) { return color(d.name); });

                      names.append("text")
                          .datum(function(d) { return {name: d.name, value: d.values[d.values.length - 1]}; })
                          .attr("transform", function(d) { return "translate(" + x(d.value.category) + "," + y(d.value.quantity) + ")"; })
                          .attr("x", 3)
                          .attr("dy", ".35em")
                          .text(function(d) { return d.name; });

                          svg.append("text")
                        .attr("x", (width / 2))             
                        .attr("y", 0 - (margin.top / 2))
                        .attr("text-anchor", "middle")
                        .style("font-family","arial black")  
                        .style("font-size", "16px")
                        .style("text-decoration", "underline")  
                        .text($('#chart_t-'+compID).val());
                    }
                });     
            });

        },
        /**console.log(response.fileURL)
         * Shows the delete button when over over deleteElems
         *
         * @param HTMLElement Can be a class or some HTMLElement
         */
        bindDelete: function(deleteElems){
            deleteElems.click(function(){
                var attachmentID = $(this).attr('data-attachid');
                var compID = $(this).attr('data-compid');
                $.ajax({
                    url: SP_AJAX_URL,
                    type: 'POST',
                    data: {action: 'DataDeleteAttachmentAJAX', nonce: SP_NONCE, attachmentID: attachmentID, compID: compID },
                    dataType  : 'json',
                    success  : function(response, statusText, jqXHR){
                        $('#Data_thumb-' + attachmentID).remove();
                    },
                    error    : function(jqXHR, statusText, errorThrown){
                            if(smartpost.sp_postComponent)
                                smartpost.sp_postComponent.showError(errorThrown);
                    }
                })
            });
        },

        /**
         * Shows the delete button when over over deleteElems. Looks for
         * the 'sp_DataDelete' class (i.e. the button to show).
         *
         * @param HTMLElement Can be a class or some HTMLElement
         */
        bindDeleteHover: function(deleteElems){
            
            deleteElems.hover(function(){
                $(this).find('.sp_DataDelete').css('display', 'block');
            }, function(){
                $(this).find('.sp_DataDelete').css('display', 'none');
            });
        },
        /**
        * Dynamically initializes a Data component
        */
 
        initComponent: function(component, postID, autoFocus){
            // var webcamLink = $(component).find('.sp_webcam_click');
            // this.webcamClick($(webcamLink));
            this.initFileDrop($(component));
            this.bindChk($(component));
        },
         
        /**
        * Statically initializes all Data components on document.ready
        */
        init: function(){
            this.setTypeID();
            var thisObj = this;
                    
            $('.sp_Data').each(function(){
                thisObj.bindChk($(this));
            });
 
            $('.sp_Data').each(function(){
                thisObj.initFileDrop($(this));
            });
         
            this.bindDelete($('.sp_DataDelete'));
            $('.gallery_thumb').each(function(){
                thisObj.bindDeleteHover($(this));
            })

            $('.sp_Data_desc').each(function(){
                thisObj.initDescEditor($(this));
            });

        }
    }

    $(document).ready(function(){
        smartpost.sp_postData.init();
    });
})(jQuery);