<?php
if (!class_exists("sp_catData")) {
	
	/**
	 * Extends sp_catComponent
	 *	Data category component. Defines administrative features for
	 * the link component. Also used alongside sp_postData for front-end
	 * handling.
	 *
	 * @see sp_catComponent
	 */

	class sp_catData extends sp_catComponent{
		
		private $defaultExts = array("txt", "csv", "json", "png");//find a way to circumvent allowing png?
		private $allowedExts = array("xlsx", "tsv");
		
		function __construct($compID = 0, $catID = 0, $name = '', 
							 $description = '', $typeID = 0, $order = 0,
    						 $options = null, $default = false, $required = false){

            $compInfo = compact("compID", "catID", "name", "description", "typeID",
                                "order", "options", "default", "required");

            if($compID == 0){
                //Set default Data options
                $options->allowedExts = array("txt" => 1, "csv" => 1, "json" => 1, "xlsx" => 0, "tsv" => 0, "svg" => 1);
               
                $this->options = $options;
            }

            $this->initComponent($compInfo);

            //Get updated data options after initializing the component
     
            $this->allowedExts = $this->options->allowedExts;
            $this->customExts  = $this->options->customExts;
		}
		
		/**
			* @see parent::installComponent()
		 */		
		function install(){
			self::installComponent('Data', 'Upload data and convert into a chart.', __FILE__);			
		}
		
		/**
			* @see parent::componentMenu()
		 */
		function componentMenu(){
			$html .= '<ul class="simpleMenu">';
                $html .= '<li class="stuffbox"><a href="#"><img src="' . IMAGE_PATH . '/downArrow.png" /></a>';
                    $html .= '<ul class="stuffbox">';
                    $html .= '<li><a href="#" class="delete_component" data-compid="' . $this->ID . '">Delete Component</a></li>';
                 $html .= '</ul>';
                $html .= '</li>';
			$html .= '</ul>';
			echo $html;	
		}
		
		/**
		 * Adds CSS / JS to stylize and handle any UI actions
		 */		
		static function init(){
			require_once('ajax/sp_catDataAJAX.php');
			sp_catDataAJAX::init();
			wp_register_script( 'sp_catDataJS', plugins_url('js/sp_catData.js', __FILE__), array('jquery', 'sp_admin_globals', 'sp_admin_js') );
			wp_enqueue_script( 'sp_catDataJS' );
		}

		/**
		 * @see parent::componentOptions()
		 */		
		function componentOptions(){
			$allowedExts = $this->allowedExts;
			

			$html .= '<p> Allowed extensions: </p>';
			foreach($this->defaultExts as $index => $ext){
				$checked = $allowedExts[$ext] ? 'checked="checked"' : '';
				$html .= '<span class="sp_extension_checkbox"><input type="checkbox" id="exts[' . $ext . ']" name="exts[' . $ext . ']" value="1" ' . $checked . ' /> <label for="exts[' . $ext . ']">' . $ext . '</label></span>';
			}
			
			// $html .= '<p>'; 
			// 	$html .= 'Add more extensions separated by commas: "zip, doc, mov"): ';
			// 	$html .= '<input type="text" class="regular-text" id="customExts" name="customExts" value="' . $customExts . '" />';
			// $html .= '</p>';
			
			// $checked = $this->isGallery ? 'checked="checked"' : '';
			// $html .= '<p><input type="checkbox" id="galleryMode" name="galleryMode" ' . $checked . ' value="1" /> Gallery - This will enable multiple file uploads and display them in grid-like gallery. </p>';
			$html .= '<button type="button" class="update_sp_Data button-secondary" data-compid="' . $this->ID . '"> Update Data </button>';
			echo $html;
		}

		/**
		 * Returns the allowed Data extensions
		 */
		function getOptions(){
			return $this->options;
		}

        /**
         * Sets the allowed extensions for the Data component
         * @param $exts an array of allowed exts: array("txt" => 1, "csv" => 1, ... )
         */
        function setExtensions($exts){
			$this->options->allowedExts = $exts;
		}

		/**
		 * Sets the option for the Data component. The $data param should be an object
		 * with a 'isGallery' boolean and 'extensions' array. @see this class definition
		 * 
		 * @param object $data
		 * @return bool True on success, null or false on failure 
		 */
		function setOptions($data){
			$options = maybe_serialize($data);
			return sp_core::updateVar('sp_catComponents', $this->ID, 'options', $options, '%s');		
		}
		
	}
}
?>