<?php
if (!class_exists("sp_postData")) {
  /**
   * Extends sp_postComponent
   *
   * @see sp_postComponent
   */  
  class sp_postData extends sp_postComponent{
      
      private $allowedExts;
    
      private $imagesAllowed = false; //whether images are enabled
      private $attachmentIDs = array(); //An array of attachment IDs
      private $galleryDesc   = ""; //Gallery description if the component is a gallery
      private $isGallery;
      
      function __construct($compID = 0, $catCompID = 0, $compOrder = 0,
                                                $name = '', $value = '', $postID = 0){
          
          $compInfo = compact("compID", "catCompID", "compOrder", "name", "value", "postID");

          //Set default options from category component for new instances      
          if($compID == 0){
            $this->options = sp_catComponent::getOptionsFromID($catCompID);
          }
          
          $this->initComponent($compInfo);
          
          //Get extensions & gallery bool
          $this->allowedExts = $this->options->allowedExts;
          
          $this->isGallery   = $this->options->isGallery;
          
          //Load instance vars
          if( !empty($this->value) ){
            $this->attachmentIDs = $this->value;
          }
          
          //Check to see if images are allowed to see if
          //We should enable webcam uploads  
          // if( !empty($this->allowedExts) ){
          //   $imgExts = array("csv" => 1, "txt" => 1, "json" => 1);
          //   $this->imagesAllowed = (bool) array_intersect_key($imgExts, $this->allowedExts);
          // }
      }
      
      /**
       * @see parent::init()
       */       
      static function init(){
        require_once('ajax/sp_postDataAJAX.php');
        sp_postDataAJAX::init();
        self::enqueueCSS();
        self::enqueueJS();
      }
      
      static function enqueueCSS(){
          wp_register_style( 'sp_postDataCSS', plugins_url('css/sp_postData.css', __FILE__) );
          wp_enqueue_style( 'sp_postDataCSS' );
      }
      
      static function enqueueJS(){
        wp_register_script( 'jquery-filedrop', plugins_url('js/jquery.filedrop.js', __FILE__) );        
        wp_register_script( 'sp_postDataJS', plugins_url('js/sp_postData.js', __FILE__) );
        wp_register_script( 'd3_JS', plugins_url('js/mbostock-d3-dddef32/d3.min.js', __FILE__) );

        wp_register_script( 'canv_JS', plugins_url('js/canvg/canvg.js', __FILE__) );
        wp_register_script( 'rgb_JS', plugins_url('js/canvg/rgbcolor.js', __FILE__) );
        wp_register_script( 'sBlur_JS', plugins_url('js/canvg/StackBlur.js', __FILE__) );

        wp_enqueue_script( 'jquery-filedrop', null, array( 'jquery' ) );         
        wp_enqueue_script( 'sp_postDataJS',  null, array( 'jquery', 'sp_globals', 'sp_postComponentJS') );
        wp_enqueue_script( 'd3_JS', null, array( 'jquery' ) );   

        wp_enqueue_script( 'canv_JS', null, array( 'jquery' ) );   
        wp_enqueue_script( 'rgb_JS', null, array( 'jquery' ) );   
        wp_enqueue_script( 'sBlur_JS', null, array( 'jquery' ) );   
      }

      /**
       * @see parent::renderEditMode()
       */      
      function renderEditMode(){
        
          $html .= '<div id="sp_Data-' . $this->ID .'" class="sp_Data" data-compid="' . $this->ID .'" data-isgallery="' . (string) $this->isGallery . '">';

          $html .= '<div id="sp_uploads-' . $this->ID . '" class="sp_uploads">';
          $html .= '<div class="form_container" id="form_container-' . $this->ID . '" ><div id="form_wrapper">';
      $html .= '<form action="drop_test.php" method="post" target="upload_target" enctype="multipart/form-data" id="chartform">';
      $html .=     '<h3 id="chart_xtitle">X-axis title:<input type="textbox" id="chart_t" name="chart_t" maxlength="35"/></h3>';
      $html .=     '<h3 id="chart_ytitle">Y-axis title:<input type="textbox" id="chart_t" name="chart_t" maxlength="35"/></h3>';
      $html .=   '<div>';
      $html .=   '<div class="button_s piechart_b" data-compid="' . $this->ID .'" id="piechart_b-' . $this->ID . '">Wheel Chart</div>';
      $html .=   '<div class="button_s barchart_b" data-compid="' . $this->ID .'" id="barchart_b-' . $this->ID . '">Bar Chart</div>';
      $html .=   '<div class="button_s linechart_b" data-compid="' . $this->ID .'" id="linechart_b-' . $this->ID . '">Line Chart</div>';
      $html .=   '</div>';
      $html .=   '<div>';
      $html .=   '<input type="checkbox" class="chart_chk large" data-compid="' . $this->ID .'" id="large-' . $this->ID . '"/>Large';
      $html .=   '<input type="checkbox" class="chart_chk medium" data-compid="' . $this->ID .'" id="medium-' . $this->ID . '"checked/>Medium';
      $html .=   '<input type="checkbox" class="chart_chk small" data-compid="' . $this->ID .'" id="small-' . $this->ID . '"/>Small';
      $html .=   '</div>';
      $html .=   '</tr>';  
      $html .=   '<tr>';    
      $html .=   '<iframe id="upload_target" name="upload_target" src="#" style="display: none"></iframe>';
      $html .=   '</tr>';
      $html .=   '<tr>';
      $html .=   '<div>';
      $html .=   '<div class="drop-zone" id="drop-zone-'. $this->ID .'">';
      $html .=   'Drop files here...';
      $html .=    '<div class="clickHere" id="clickHere-'. $this->ID .'">';
      $html .=    'or just click!';
      $html .=    '<input type="file" data-compid="' . $this->ID .'" class="the_file" id="sp_upload-' . $this->ID . '"/>';
      $html .=    '<input type="hidden" name="formID" class="formID"  value="' . $this->ID . '" />';
      $html .=    '</div>';
      $html .=   '</div>';
      $html .=   '</div>';
      $html .=   '</tr>';        
      $html .=   '<tr>';
      $html .=   '<div class="result"></div>  ';            
      $html .=   '</tr>';    
      $html .=   '<tr>';
      // $html .=   '  <div>';
      // $html .=   '<div class="button_s done_b" style="display:inline-block" id="done_b-'.$this->ID.'" > Done Editing </div>';
      // $html .=   '<div class="button_s remove_chart" id="remove_chart-'.$this->ID.'"style="display:inline-block"> Remove Chart </div>';
      // $html .=   '  </div>';
      $html .=   '</tr>';    
      $html .=   '</form>';  
      $html .= '</div></br></div><div class="chart_wrapper" id="chart_wrapper-'.$this->ID.'" ></div> <input type="file" style="DISPLAY: none"  data-compid="' . $this->ID .'"  value="" id="sp_Chartupload-' . $this->ID . '"/>';
       $html .= '</div>';
       $html .= '<div id="sp_attachments-' . $this->ID . '" class="sp_attachments">';          
            if( !empty($this->attachmentIDs) ){
                foreach($this->attachmentIDs as $id){
                  $html .= self::renderSingleThumb($id, $this->ID, $this->isGallery);
                }
            }          
            
            // if( !$this->isGallery && !empty($this->attachmentIDs) ){
            //   $html .= self::renderSingleDesc($this->attachmentIDs[0], $this->ID);
            // }
          $html .= '</div>';
          
          $html .= '<div class="clear"></div>';



        $html .= '</div>';
        return $html;
      }
      
      /**
       * @see parent::renderViewMode()
       */      
      function renderViewMode(){
        $html .= '<div id="sp_Data-' . $this->ID .'" class="sp_Data" data-isgallery="' . (string) $this->isGallery . '">';
          $html .= '<div id="sp_attachments-' . $this->ID . '" class="sp_attachments">';          
            if( !empty($this->attachmentIDs) ){
                foreach($this->attachmentIDs as $id){
                  $html .= self::renderSingleThumb($id, $this->ID, $this->isGallery, false);
                }
            }
            
            if( !$this->isGallery && !empty($this->attachmentIDs) ){
              $html .= self::renderSingleDesc($this->attachmentIDs[0], $this->ID, false);
            }
          $html .= '</div>';
            
          $html .= '<div class="clear"></div>';
          $html .= '</div>';
        return $html;    
      }      

      /**
       * Renders single attachment description
       */
      static function renderSingleDesc($id, $compID, $editMode = true){
        $html = "";
        if ( !empty($id) )
            $attachment = get_post($id);
        
        if( !empty($attachment) ){
          $descPlaceholder = $editMode ? 'Click to add a description' : '';
          $editable        = $editMode ? 'editable' : '';
          $html .= '<div id="sp_Data_desc-' . $id .'" class="sp_Data_desc ' . $editable . '" data-compid="' . $compID . '" attach-id="' . $attachment->ID . '">';
            $html .= empty( $attachment->post_content ) ? $descPlaceholder : $attachment->post_content;
          $html .= '</div>';
        }
        
        return $html;
      }

      /**
       * Renders a single attachment thumbnail
       */
      static function renderSingleThumb($id, $compID, $gallery = false, $editMode = true){
        $attachment = get_post($id);
        $html = "";
        if( !is_null($attachment) ){
          $class = $gallery ? 'gallery_thumb' : 'sp_Data_thumb';
          $html .= '<div id="Data_thumb-' . $attachment->ID . '" data-compid="' . $compID . '" class="' . $class . '">';
            
            if( strpos($attachment->post_mime_type, 'image') !== false && $gallery ){
              $hrefAttrs = 'class="fancybox" rel="gallery-' . $compID .'" title="' .$attachment->post_title . '" ';
            }
            
            $html .= '<a href="' . wp_get_attachment_url($id) . '"' . $hrefAttrs .'>';
              $html .= wp_get_attachment_image($id, array(100, 60), true);
            $html .= '</a>';
            
            //$html .= '<p id="Data_caption-' . $attachment->ID . '" class="sp_DataCaption">' . $attachment->post_title . '</p>';
            //$html .= $editMode ? '<img src="' . IMAGE_PATH . '/no.png" id="deleteThumb-' . $id .'" name="deleteThumb-' . $id .'" data-attachid="' . $id . '" data-compid="' . $compID . '" class="sp_DataDelete" alt="Delete Attachment" title="Delete Attachment">' : '';
          $html .= '</div>';
        }
        return $html;
      }
      
      function renderPreview(){
        //!To-do: include Data description
        if(!$this->isGallery){
          $attachmentIDs = $this->attachmentIDs;
          $attachment    = get_post($attachmentIDs[0]);
          $html = $attachment->post_content;
        }else{
          $html = $this->galleryDesc;
        }
        
        return $html;
      }

      /**
       * @see parent::addMenuOptions()
       */      
      function addMenuOptions(){
        return array();
      }
      
      /**
       * Overload parent function since we need to delete all the attachments
       * As we delete all the attachments
       *
       * @return bool|int false on failure, number of rows affected on success
       */
      function delete(){
        global $wpdb;
        
        if( !empty($this->attachmentIDs) ){
          foreach($this->attachmentIDs as $id){
            if(get_post_thumbnail_id($this->postID) == $id){
              
            }
            wp_delete_attachment($id, true);
          }
        }
        
        $tableName = $wpdb->prefix . 'sp_postComponents';
        return $wpdb->query(
          $wpdb->prepare(
            "DELETE FROM $tableName
             WHERE id = %d",
            $this->ID
         )
        );
      }      
      
      /**
       * Changes the acceptable extensions of the
       *
       * @param array $extensions Extensions of the form array( ".jpg" => 0|1 ),
       *                                                    where 1 is enabled, and 0 is disabled
       */
      function update($extensions){}
      
      function isEmpty(){
        return empty($this->attachmentIDs);
      }
      
      function getExtensions(){
        return $this->allowedExts;
      }
      
      function getCustomExts(){
        return $this->customExts;
      }
      
      function isGallery(){
        return $this->isGallery;
      }
      
      function getDescription($id){
        $attachment = get_post($id);
        return $attachment->post_content;
      }
      
      /**
       * Sets the description of a particular attachment
       *
       * @param string $description The description of the attachment
       * @param int $attachmentID The ID of the attachment
       */
      static function setDescription($description, $attachmentID){
        $attachment = get_post($attachmentID);
        $attachment->post_content = $description;
        return wp_update_post($attachment);
      }
      
      function getAttachments(){
        return $this->attachmentIDs;
      }
      
      /**
       * Sets the attachmentIDs. $idArray should be of the form array( 0 => id1, 1 => id2, etc.. )
       *
       * @param array $idArray An array contain attachment IDs
       * @return bool True if update was succesful, otherwise false
       */
      function setAttachmentIDs($idArray){
        
        if( !empty($idArray) ){
          $this->attachmentIDs = $idArray;
        }else{
          $this->attachmentIDs = array();
        }
        $attachmentIDs = maybe_serialize($this->attachmentIDs);
        return sp_core::updateVar('sp_postComponents', $this->ID, 'value', $attachmentIDs, '%s');      
      }
  }
}
      
?>