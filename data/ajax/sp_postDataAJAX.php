<?php
/*
 * AJAX-Related functions for all
 * sp_postData components. Functions are used
 * in front end posts.
 */

if (!class_exists("sp_postDataAJAX")) {
	class sp_postDataAJAX{
		
		static function init(){
			add_action('wp_ajax_saveDataDescAJAX', array('sp_postDataAJAX', 'saveDataDescAJAX'));		
			add_action('wp_ajax_DataUploadAJAX', array('sp_postDataAJAX', 'DataUploadAJAX'));
			add_action('wp_ajax_DataDeleteAttachmentAJAX', array('sp_postDataAJAX', 'DataDeleteAttachmentAJAX'));			
		}
		function saveChartAJAX(){

			$nonce = $_POST['nonce'];
				if( !wp_verify_nonce($nonce, 'sp_nonce') ){
					header("HTTP/1.0 403 Security Check.");
					die('Security Check');
				}

				if(!class_exists('sp_postData')){
						header("HTTP/1.0 409 Could not instantiate sp_postData class.");
						echo json_encode(array('error' => 'Could not save link.'));						
				}
				
				if( empty($_POST['compID']) ){
					header("HTTP/1.0 409 Could find component ID to udpate.");
					exit;					
				}
				$compID		       = (int) $_POST['compID'];
				$attachmentID   = (int)  $_POST['attachmentID'];
				$DataComponent = new sp_postData($compID);

		}
		function saveDataDescAJAX(){
		
				$nonce = $_POST['nonce'];
				if( !wp_verify_nonce($nonce, 'sp_nonce') ){
					header("HTTP/1.0 403 Security Check.");
					die('Security Check');
				}

				if(!class_exists('sp_postData')){
						header("HTTP/1.0 409 Could not instantiate sp_postData class.");
						echo json_encode(array('error' => 'Could not save link.'));						
				}
				
				if( empty($_POST['compID']) ){
					header("HTTP/1.0 409 Could find component ID to udpate.");
					exit;					
				}
					
				$compID		       = (int) $_POST['compID'];
				$attachmentID   = (int)  $_POST['attachmentID'];
				$desc				       = (string) stripslashes_deep($_POST['desc']);	
				$DataComponent = new sp_postData($compID);
	
				if( is_wp_error($linkComponent->errors) ){
					header("HTTP/1.0 409 " . $success->get_error_message());
				}else{
					$success = $DataComponent->setDescription($desc, $attachmentID);
					if($success === false){
						header("HTTP/1.0 409 Could not save link description.");
					}else{
						echo json_encode(array('success' => true));
					}
					
				}
				exit;
		}		
		
		static function DataDeleteAttachmentAJAX(){
				$nonce = $_POST['nonce'];
			 if( !wp_verify_nonce($nonce, 'sp_nonce') ){
					header("HTTP/1.0 403 Security Check.");
					die('Security Check');
				}
				
				if(!class_exists('sp_postData')){
						header("HTTP/1.0 409 Could not instantiate sp_postData class.");
						exit;
				}	

				if( empty($_POST['compID']) ){
					header("HTTP/1.0 409 Could find component ID to udpate.");
					exit;
				}
				
				if( empty($_POST['attachmentID']) ){
					header("HTTP/1.0 409 Could find attachment ID to udpate.");
					exit;
				}
				
				$id			= (int) $_POST['attachmentID'];
				$compID       = (int) $_POST['compID'];
				$DataComponent = new sp_postData($compID);
				$postThumbID    = get_post_thumbnail_id($DataComponent->getPostID());				
				$attachmentIDs  = $DataComponent->getAttachments();
				
				//Delete the attachment
				wp_delete_attachment( $id, true );
				$idKey = array_search($id, $attachmentIDs);
				
				if( !empty($idKey) )
						unset( $attachmentIDs[$idKey] );
				
				$success = $DataComponent->setAttachmentIDs( $attachmentIDs );
				if( $success === false ){
					header("HTTP/1.0 409 Could not successfully set attachment ID.");
					exit;					
				}
				
				//Set the post thumb if the one that's deleted is the featured image
				if($postThumbID == $id){
					$args  = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_parent' => $post->ID, 'post_status' => null );
					$attachments = get_posts($args);
					if($attachments){
						set_post_thumbnail($DataComponent->getPostID(), $attachments[0]->ID);
					}
				}
				
				echo json_encode( array('sucess' => true) );
				exit;
		}

		// upload chart svg 
		static function ChartUploadAJAX(){		
				$nonce = $_POST['nonce'];
				if( !wp_verify_nonce($nonce, 'sp_nonce') ){
					header("HTTP/1.0 403 Security Check.");
					die('Security Check');
				}
				
				if(!class_exists('sp_postData')){
						header("HTTP/1.0 409 Could not instantiate sp_postData class.");
						exit;
				}	

				if( empty($_POST['compID']) ){
					header("HTTP/1.0 409 Could find component ID to udpate.");
					exit;
				}
		
				if(empty($_FILES)){
						header("HTTP/1.0 409 Files uploaded are empty!");	
						exit;
				}
				
				$compID = (int) $_POST['compID'];
				$svgcode = $_POST['json'];
		}

		static function DataUploadAJAX(){		
				$nonce = $_POST['nonce'];
				if( !wp_verify_nonce($nonce, 'sp_nonce') ){
					header("HTTP/1.0 403 Security Check.");
					die('Security Check');
				}
				
				if(!class_exists('sp_postData')){
						header("HTTP/1.0 409 Could not instantiate sp_postData class.");
						exit;
				}	

				if( empty($_POST['compID']) ){
					header("HTTP/1.0 409 Could find component ID to udpate.");
					exit;
				}
		
				if(empty($_FILES)){
						header("HTTP/1.0 409 Files uploaded are empty!");	
						exit;
				}
				
				$compID = (int) $_POST['compID'];
				$DataComponent = new sp_postData($compID);
				$postID = $DataComponent->getPostID();
				
				$defaultExts = $DataComponent->getExtensions();
				$customExts  = $DataComponent->getCustomExts();
				$defaultExts = !empty($defaultExts) ? array_keys($DataComponent->getExtensions()) : array();
				$customExts  = !empty($customExts)  ? array_keys($DataComponent->getCustomExts()) : array();
	
				$allowedExts = array_merge($defaultExts, $customExts);
				$allowed = sp_core::validateExtension($_FILES['sp_Data_files']['name'], $allowedExts);
				
				//$description = $DataComponent->getDescription();
				if($allowed){
					$caption = $_FILES['sp_Data_files']['name'];
					$id = sp_core::upload($_FILES, 'sp_Data_files', $postID, array('post_title' => $caption));
				}else{
					header("HTTP/1.0 409 File type not allowed.");
					exit;
				}
				
				if(is_wp_error($id)){
					header("HTTP/1.0 409 Could not successfully upload file, " . $id->get_error_message());
					exit;
				}

				$attachmentIDs = $DataComponent->getAttachments();

				//Delete the previous attachment if it's not a gallery
				if( !$DataComponent->isGallery() ){
					if( !empty($attachmentIDs) ){
						wp_delete_attachment($attachmentIDs[0]);
					}
					$attachmentIDs[0] = $id;					
				}else{ //Otherwise if it's a gallery add the new attachment to the attachmentIDs array
					array_push($attachmentIDs, $id);				
				}
				
				$success = $DataComponent->setAttachmentIDs($attachmentIDs);
				if( $success === false ){
					header("HTTP/1.0 409 Could not successfully set attachment ID.");
					exit;					
				}
				
				//Set featured image if it's not already set
				if( !has_post_thumbnail($postID) ){
					set_post_thumbnail($postID, $id);
				}
				if($allowed[0]  !== 'json'){


				}
				self::attachmentJSON($id);
				
				exit;
		}
			
		/**
		 * Echo out JSON info about an attachment
		 */
		static function attachmentJSON($id){
			$attachment = get_post($id);
			$fileURL = wp_get_attachment_url($id);
			$thumb   = wp_get_attachment_image_src( $id, array(100, 60), true );
			$caption = $attachment->post_title;
			echo json_encode( array('id' => $id, 'fileURL' => $fileURL, 'thumbURL' => $thumb, 'caption' => $caption) );		
		}
		
	}
}
?>